import React from 'react';
import {render, screen, within} from '@testing-library/react-native';
import TagBlock from '@components/molecules/TagBlock';

const data = {
  id: '01',
  firstName: 'Dev',
};

describe('TagBlock Components', () => {
  it('user should be firstName"', async () => {
    render(
      <TagBlock handleRightClick={() => console.log('hallo')} {...data} />,
    );
    const Label = within(screen.getByText('Dev'));
    expect(Label).toBeTruthy();
  });
});
