import React from 'react';
import {Text} from 'react-native';
import {render, screen, within} from '@testing-library/react-native';
import CustomButton from '@components/atoms/CustomButton';

describe('Custom button components test render', () => {
  it('should button have a text "test"', async () => {
    render(
      <CustomButton testID="test-custom-button">
        <Text>Hello World!</Text>
      </CustomButton>,
    );
    const Button = within(screen.getByTestId('test-custom-button'));
    expect(Button).toBeTruthy();
  });
});
