import {REGEX} from 'src/constants/regexList';

/**
 * Function for checking valid url or not
 * @param url is string;
 * @returns boolean;
 * @example checkValidUrlImage('https:***.jpg') and function will return true
 * @example checkValidUrlImage('N/A') and function will return false
 */
const checkValidUrlImage = (url: string) => {
  return REGEX.validationUrl.test(url);
};

export {checkValidUrlImage};
