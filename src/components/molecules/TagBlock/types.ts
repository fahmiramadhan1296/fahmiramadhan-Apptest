export interface ISideData {
  handleClick: (id?: string) => void;
  iconName?: string;
}

export interface ITagBlockProps {
  id?: string;
  firstName?: string;
  photo?: string;
  handleRightClick: (id?: string) => void;
  sideData?: ISideData[];
}
