import React from 'react';
import {View} from 'react-native';
import {styles} from './styles';
import {CustomText} from '@components/atoms/Text/text';
import {CustomIcon} from '@components/atoms/Icons/icon';
import {ITagBlockProps} from './types';
import CustomAvatar from '@components/atoms/Avatar';
import CustomButton from '@components/atoms/CustomButton';

const TagBlock: React.FC<ITagBlockProps> = ({
  id,
  firstName,
  photo,
  handleRightClick,
  sideData,
}) => {
  return (
    <View style={styles.container} key={id}>
      <CustomButton onPress={() => handleRightClick(id)}>
        <View style={styles.sideContainer}>
          <CustomAvatar size={24} imgUrl={photo ?? ''} />
          <CustomText style={styles.text}>{firstName}</CustomText>
        </View>
      </CustomButton>
      <View style={styles.sideContainer}>
        {sideData?.map((item, index) => (
          <CustomIcon
            key={index}
            style={styles.iconStyle}
            name={item.iconName ?? ''}
            onPress={() => item.handleClick(id)}
          />
        ))}
      </View>
    </View>
  );
};

export {TagBlock};
