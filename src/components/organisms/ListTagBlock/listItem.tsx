/**
 * For list item component we use flat list for display ui.
 * For the optimize render.
 * If you want more documentation you can check in here: https://reactnative.dev/docs/using-a-listview
 */
import TagBlock from '@components/molecules/TagBlock';
import {ITagBlockProps} from '@components/molecules/TagBlock/types';
import React from 'react';
import {FlatList, View} from 'react-native';
import {styles} from './styles';

type IListItemProps = {
  data: any;
};

const renderItem = (props: ITagBlockProps) => {
  return (
    <View style={styles.item}>
      <TagBlock {...props} />
    </View>
  );
};

const ListTagBlock: React.FC<IListItemProps> = ({data}) => {
  return (
    <View style={styles.container}>
      {data && (
        <FlatList data={data} renderItem={({item}) => renderItem(item)} />
      )}
    </View>
  );
};

export {ListTagBlock};
