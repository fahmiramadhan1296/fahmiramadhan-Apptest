import {Dimensions, StyleSheet} from 'react-native';
import {COLOR} from 'src/themes/colors';

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    bottom: 6,
    borderRadius: 12,
    marginHorizontal: Dimensions.get('window').width * 0.1,
    justifyContent: 'space-evenly',
    backgroundColor: COLOR.MONOCHROME_1,
    borderColor: COLOR.POLYCHROME_10,
    borderWidth: 1,
  },
  mainItemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    borderRadius: 20,
    borderColor: COLOR.POLYCHROME_10,
    borderWidth: 1,
    padding: 4,
  },
});

export {styles};
