import React, {ReactNode} from 'react';
import {View} from 'react-native';
import CustomButton from '@components/atoms/CustomButton';
import {styles} from './styles';

type TTabItems = {
  id: string;
  handleClick: () => void;
  text: string;
  icon: ReactNode;
};

type TTabBar = {
  tabItems: TTabItems[];
};

const TabBar = ({tabItems}: TTabBar) => {
  return (
    <View style={styles.mainContainer}>
      {tabItems &&
        tabItems.map((item: TTabItems) => {
          return (
            <View key={item.id} style={styles.mainItemContainer}>
              <CustomButton onPress={item.handleClick}>
                {item.icon}
              </CustomButton>
            </View>
          );
        })}
    </View>
  );
};

export {TabBar};
