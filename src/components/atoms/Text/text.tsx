import React from 'react';
import {Text, TextProps} from 'react-native';

const CustomText: React.FC<TextProps> = props => {
  return <Text {...props}>{props.children}</Text>;
};

export {CustomText};
