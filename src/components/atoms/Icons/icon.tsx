/**
 * Icon using react native icon and Mui icon.
 * If want to change icon with another library you can check in https://github.com/oblador/react-native-vector-icons
 * And you can check list icon in here : https://fonts.google.com/icons
 */

import React from 'react';
import {IconProps} from 'react-native-vector-icons/Icon';
import Icon from 'react-native-vector-icons/MaterialIcons';

const CustomIcon: React.FC<IconProps> = props => {
  return <Icon {...props} />;
};

export {CustomIcon};
