import {Input, InputProps} from '@rneui/themed';
import React from 'react';

const CustomInput: React.FC<InputProps> = props => {
  return <Input {...props} />;
};

export {CustomInput};
