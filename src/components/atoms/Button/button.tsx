/**
 * Button using react native element
 * if you want more documentation yuo can check in here: https://reactnativeelements.com/docs/components/button
 */
import React from 'react';
import {Button, ButtonProps} from '@rneui/themed';

const NormalButton: React.FC<ButtonProps> = props => {
  return <Button {...props} />;
};

export {NormalButton};
