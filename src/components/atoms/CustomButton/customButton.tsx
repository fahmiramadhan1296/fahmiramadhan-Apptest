import React from 'react';
import {TouchableOpacity, TouchableOpacityProps} from 'react-native';

const CustomButton: React.FC<TouchableOpacityProps> = ({
  children,
  ...props
}) => {
  return <TouchableOpacity {...props}>{children}</TouchableOpacity>;
};

export {CustomButton};
