import React from 'react';
import {Dialog} from '@rneui/themed';
import {CustomText} from '../Text/text';

interface IDialogProps {
  isLoading?: boolean;
  isVisible?: boolean;
  title?: string;
  description?: string;
  handleDismissDialog?: () => void;
}

const CustomDialog: React.FC<IDialogProps> = ({
  isVisible,
  title,
  description,
  isLoading,
  handleDismissDialog,
}) => {
  return (
    <Dialog isVisible={isVisible} onBackdropPress={handleDismissDialog}>
      {isLoading && <Dialog.Loading />}
      {!isLoading && (
        <>
          <Dialog.Title title={title} />
          <CustomText>{description}</CustomText>
          <Dialog.Actions>
            <Dialog.Button title="Ok" onPress={handleDismissDialog} />
          </Dialog.Actions>
        </>
      )}
    </Dialog>
  );
};

export {CustomDialog};
