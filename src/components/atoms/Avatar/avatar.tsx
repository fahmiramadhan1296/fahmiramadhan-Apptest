/**
 * For this components extend from react native element.
 * You want to check documentation in here : https://reactnativeelements.com/docs/components/avatar
 */

import React from 'react';
import {Avatar, AvatarProps} from '@rneui/themed';
import {checkValidUrlImage} from 'src/helpers/checkValidUrl';
import {COLOR} from 'src/themes/colors';

interface IAvatarProps extends AvatarProps {
  imgUrl: string;
}

const CustomAvatar: React.FC<IAvatarProps> = ({imgUrl, ...props}) => {
  return (
    <Avatar
      rounded
      {...(checkValidUrlImage(imgUrl) && {source: {uri: imgUrl}})}
      {...(!checkValidUrlImage(imgUrl) && {icon: {name: 'call', type: 'mui'}})}
      containerStyle={{
        backgroundColor: COLOR.POLYCHROME_10,
      }}
      {...props}
    />
  );
};

export {CustomAvatar};
