import React, {ReactNode} from 'react';
import TabBar from '@components/organisms/TabBar';
import {CustomIcon} from '@components/atoms/Icons/icon';
import {styles} from './styles';

const iconTabView: {[key: string]: ReactNode} = {
  home: <CustomIcon style={styles.iconStyle} name="home" />,
  modify: <CustomIcon style={styles.iconStyle} name="add" />,
  recycle: <CustomIcon style={styles.iconStyle} name="recycling" />,
};

const TabView = ({state, descriptors, navigation}: any) => {
  const itemsTab = state.routes.map((route: any, index: number) => {
    const {options} = descriptors[route.key];
    let label = '';
    switch (true) {
      case options.tabBarLabel !== undefined:
        label = options.tabBarLabel;
        break;
      case options.title !== undefined:
        label = options.title;
        break;
      default:
        label = route.name;
        break;
    }

    const isFocused = state.index === index;
    const handleOnPress = () => {
      const event = navigation.emit({
        type: 'tabPress',
        target: route.key,
        canPreventDefault: true,
      });

      if (!isFocused && !event.defaultPrevented) {
        navigation.navigate({name: route.name}, {id: '', isUpdate: false});
      }
    };

    return {
      id: `${label}-${index}`,
      text: label,
      handleClick: handleOnPress,
      icon: iconTabView[label.toLowerCase()],
    };
  });
  return <TabBar tabItems={itemsTab} />;
};

export {TabView};
