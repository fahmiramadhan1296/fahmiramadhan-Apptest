import {StyleSheet} from 'react-native';
import {COLOR} from 'src/themes/colors';
import {FONT_SIZE} from 'src/themes/fontSize';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 12,
    backgroundColor: COLOR.POLYCHROME_3,
    borderRadius: 5,
  },
  sideContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    gap: 8,
    alignItems: 'center',
  },
  iconStyle: {
    fontSize: FONT_SIZE.XL,
  },
  text: {
    fontSize: FONT_SIZE.L,
    color: COLOR.POLYCHROME_10,
  },
});

export {styles};
