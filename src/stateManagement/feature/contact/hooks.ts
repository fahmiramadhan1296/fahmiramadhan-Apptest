/**
 * Custom hooks for manage Redux
 */

import {useDispatch, useSelector} from 'react-redux';
import {RootState} from 'src/stateManagement/store';
import {setRecyclingData} from './reducer';
import {IContactState} from './types';
import {
  useAddContactMutation,
  useDeleteContactMutation,
  useGetContactDetailQuery,
  useGetContactQuery,
  useUpdateContactMutation,
} from './services';

interface IUseContactProps {
  id?: any;
  refetchOnMountOrArgChange?: number;
}

const useContact = (props?: IUseContactProps) => {
  const {
    data: contactDataApi,
    error,
    isLoading,
  } = useGetContactQuery(props?.id, {refetchOnMountOrArgChange: true});

  const {
    data: contactDetails,
    error: contactDetailsError,
    isLoading: contactDetailsIsLoading,
  } = useGetContactDetailQuery(props?.id, {refetchOnMountOrArgChange: true});

  const [addNewContact, responseAddNewContact] = useAddContactMutation();
  const [deleteContact, responseDeleteContact] = useDeleteContactMutation();
  const [updateContact, responseUpdateContact] = useUpdateContactMutation();

  const dispatch = useDispatch();
  const recyclingData = useSelector(
    (state: RootState) => state.contactReducer.recyclingData,
  );

  const setHookRecyclingData = (data?: IContactState) => {
    dispatch(setRecyclingData([...recyclingData, data]));
  };

  const removeRecyclingData = (id?: string) => {
    dispatch(
      setRecyclingData(
        recyclingData.filter(
          ({id: recyclingId}: IContactState) => recyclingId !== id,
        ),
      ),
    );
  };

  return {
    setRecyclingData: setHookRecyclingData,
    removeRecyclingData: removeRecyclingData,
    getRecyclingData: recyclingData,
    getContactDataService: {
      data: contactDataApi?.data,
      error,
      isLoading,
    },
    getContactDataDetailService: {
      data: contactDetails?.data,
      error: contactDetailsError,
      isLoading: contactDetailsIsLoading,
    },
    setContactServiceData: {
      addNewContact,
      response: responseAddNewContact,
    },
    updateContactServiceData: {
      updateContact,
      response: responseUpdateContact,
    },
    deleteContact: {
      deleteContact,
      responseDeleteContact,
    },
  };
};

export default useContact;
