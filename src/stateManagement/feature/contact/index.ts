import reducer from './reducer';
import {contactApi} from './services';

const reducerContact = {
  contactReducer: reducer,
  [contactApi.reducerPath]: contactApi.reducer,
};

export default reducerContact;
