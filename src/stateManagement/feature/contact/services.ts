import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import {API_LIST} from 'src/constants/apiList';
import {IContactState, IGetContactApi, IGetContactDetailApi} from './types';

export const contactApi = createApi({
  reducerPath: 'contactApi',
  baseQuery: fetchBaseQuery({baseUrl: API_LIST.BASE_URL}),
  tagTypes: ['Post'],
  endpoints: builder => ({
    getContact: builder.query<IGetContactApi, string>({
      query: () => API_LIST.CONTACT,
    }),
    getContactDetail: builder.query<IGetContactDetailApi, string>({
      query: id => {
        return `${API_LIST.CONTACT}/${id}`;
      },
    }),
    addContact: builder.mutation<IGetContactDetailApi, {}>({
      query: payload => {
        return {
          url: API_LIST.CONTACT,
          method: 'POST',
          body: payload,
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        };
      },
    }),
    updateContact: builder.mutation<
      void,
      Pick<IContactState, 'id'> & Partial<IContactState>
    >({
      query: ({id, ...patch}) => {
        console.log(id);
        return {
          url: `${API_LIST.CONTACT}/${id}`,
          method: 'PUT',
          body: patch,
        };
      },
    }),
    deleteContact: builder.mutation<{success: boolean; id: string}, string>({
      query(id) {
        return {
          url: `${API_LIST.CONTACT}/${id}`,
          method: 'DELETE',
        };
      },
    }),
  }),
});

export const {
  useGetContactQuery,
  useGetContactDetailQuery,
  useAddContactMutation,
  useDeleteContactMutation,
  useUpdateContactMutation,
} = contactApi;
