export interface IContactState {
  firstName: string;
  lastName: string;
  photo: string;
  age?: number | string;
  id?: string;
}

export interface IGetContactApi {
  data: IContactState[];
  message: string;
}

export interface IGetContactDetailApi {
  data: IContactState;
  message: string;
}
