import {createSlice} from '@reduxjs/toolkit';
import type {PayloadAction} from '@reduxjs/toolkit';
import {IContactState} from './types';

interface ContactData {
  recyclingData: IContactState[];
}

const initialState: ContactData = {
  recyclingData: [],
};

export const contactSlice = createSlice({
  name: 'contact',
  initialState,
  reducers: {
    setRecyclingData: (state, action: PayloadAction<IContactState[]>) => {
      state.recyclingData = action.payload;
    },
  },
});

export const {setRecyclingData} = contactSlice.actions;
export default contactSlice.reducer;
