export const COLOR = {
  MONOCHROME_0: '#ffffff',
  MONOCHROME_1: '#fffffe',

  POLYCHROME_1: '#fef6e4',
  POLYCHROME_2: '#8bd3dd',
  POLYCHROME_3: '#f3c9b4',
  POLYCHROME_4: '#f17eaa',
  POLYCHROME_5: '#82f5ba',
  POLYCHROME_6: '#243c5a',
  POLYCHROME_7: '#242a47',
  POLYCHROME_8: '#b8c1ec',
  POLYCHROME_9: '#fabfc8',
  POLYCHROME_10: '#001858',
};
