const FONT_SIZE = {
  S: 10,
  M: 12,
  L: 14,
  XL: 18,
};

export {FONT_SIZE};
