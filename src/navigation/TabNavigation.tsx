import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from 'src/pages/home';
import TabView from 'src/app-components/TabBar';
import Modify from 'src/pages/modify';
import Recycling from 'src/pages/recycling';

const Tab = createBottomTabNavigator();

const TabNavigation = () => {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      tabBar={props => <TabView {...props} />}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{unmountOnBlur: true}}
      />
      <Tab.Screen
        name="modify"
        component={Modify}
        options={{unmountOnBlur: true}}
      />
      <Tab.Screen
        name="recycle"
        component={Recycling}
        options={{unmountOnBlur: true}}
      />
    </Tab.Navigator>
  );
};

export default TabNavigation;
