import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import TabBarNavigation from './TabNavigation';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Navigation = () => {
  const RootStack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <RootStack.Navigator screenOptions={{headerShown: false}}>
        <RootStack.Screen name="tab" component={TabBarNavigation} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
