import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    padding: 12,
    height: Dimensions.get('screen').height,
  },
});

export {styles};
