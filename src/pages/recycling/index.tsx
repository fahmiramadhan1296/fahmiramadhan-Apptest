import React, {useCallback, useMemo} from 'react';
import {SafeAreaView, View} from 'react-native';
import ListTagBlock from '@components/organisms/ListTagBlock';
import {styles} from './styles';
import useContact from 'src/stateManagement/feature/contact/hooks';

const Recycling = () => {
  const useContactHook = useContact();

  const handleRecycle = useCallback(
    (id: string) => {
      useContactHook.removeRecyclingData(id);
    },
    [useContactHook],
  );

  const nContactData = useMemo(() => {
    let cData = useContactHook.getRecyclingData?.map((data: any) => {
      return {
        ...data,
        sideData: [
          {
            iconName: 'recycling',
            handleClick: handleRecycle,
          },
        ],
      };
    });
    cData?.sort(({prev, next}: any) => {
      if (prev.firstName < next.firstName) {
        return -1;
      }
      if (prev.firstName > next.firstName) {
        return 1;
      }
      return 0;
    });
    return cData;
  }, [handleRecycle, useContactHook.getRecyclingData]);

  return (
    <SafeAreaView>
      <View style={styles.container}>
        {nContactData && <ListTagBlock data={[...nContactData]} />}
      </View>
    </SafeAreaView>
  );
};

export default Recycling;
