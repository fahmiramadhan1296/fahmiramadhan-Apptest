import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {SafeAreaView, View} from 'react-native';
import ListTagBlock from '@components/organisms/ListTagBlock';
import {styles} from './styles';
import useContact from 'src/stateManagement/feature/contact/hooks';
import {FetchBaseQueryError} from '@reduxjs/toolkit/dist/query';
import {SerializedError} from '@reduxjs/toolkit';
import {IContactState} from 'src/stateManagement/feature/contact/types';
import {CustomDialog} from '@components/atoms/Modal/modal';

const Home = ({route, navigation}: any) => {
  const {id: paramsId} = route.params ?? {id: ''};
  const {getContactDataService, deleteContact, setRecyclingData} = useContact({
    id: paramsId,
  });
  const {
    data: contactData,
  }: {
    data: IContactState[] | undefined;
    error: FetchBaseQueryError | SerializedError | undefined;
    isLoading: boolean;
  } = getContactDataService;

  const handleDetailClick = useCallback(
    (id: string) => {
      navigation.navigate('modify', {id, isUpdate: true});
    },
    [navigation],
  );

  const handleDelete = useCallback(
    (id: string) => {
      deleteContact.deleteContact(id);
      setRecyclingData(
        contactData?.find(item => {
          return item.id === id;
        }),
      );
    },
    [contactData, deleteContact, setRecyclingData],
  );

  const nContactData = useMemo(() => {
    let cData = contactData?.map(data => {
      return {
        ...data,
        handleRightClick: handleDetailClick,
        sideData: [
          {
            iconName: 'delete',
            handleClick: handleDelete,
          },
        ],
      };
    });
    cData?.sort((prev, next) => {
      if (prev.firstName < next.firstName) {
        return -1;
      }
      if (prev.firstName > next.firstName) {
        return 1;
      }
      return 0;
    });
    return cData;
  }, [contactData, handleDelete, handleDetailClick]);

  const [isVisibleModal, setVisibleModal] = useState(false);

  useEffect(() => {
    setVisibleModal(deleteContact.responseDeleteContact.isError);
  }, [deleteContact.responseDeleteContact.isError]);

  return (
    <SafeAreaView>
      <CustomDialog
        isLoading={
          getContactDataService.isLoading ||
          deleteContact.responseDeleteContact.isLoading
        }
        isVisible={isVisibleModal}
        title={
          deleteContact.responseDeleteContact.isError ? 'ERROR DELETE DATA' : ''
        }
        description={
          deleteContact.responseDeleteContact.isError
            ? 'error delete data please try again later'
            : ''
        }
        handleDismissDialog={() => setVisibleModal(false)}
      />
      <View style={styles.container}>
        {nContactData && <ListTagBlock data={[...nContactData]} />}
      </View>
    </SafeAreaView>
  );
};

export default Home;
