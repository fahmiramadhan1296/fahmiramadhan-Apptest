import {Dimensions, StyleSheet} from 'react-native';
import {COLOR} from 'src/themes/colors';

const styles = StyleSheet.create({
  container: {
    padding: 12,
    height: Dimensions.get('screen').height,
  },
  iconAddContainer: {
    position: 'absolute',
    bottom: 160,
    right: 20,
    backgroundColor: COLOR.POLYCHROME_5,
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    borderRadius: 20,
  },
  iconAdd: {
    color: COLOR.POLYCHROME_10,
  },
});

export {styles};
