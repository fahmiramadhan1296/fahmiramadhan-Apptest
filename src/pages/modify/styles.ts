import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  buttonContainer: {
    padding: 12,
    flexDirection: 'column',
    gap: 12,
  },
});

export {styles};
