import React, {useEffect} from 'react';
import {View} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import {CustomInput} from '@components/atoms/Input/customInput';
import NormalButton from '@components/atoms/Button';
import {styles} from './styles';
import useContact from 'src/stateManagement/feature/contact/hooks';
import {CustomDialog} from '@components/atoms/Modal/modal';

const defaultValue = {
  firstName: '',
  lastName: '',
  age: '',
  photo: '',
};

export default function Modify({route, navigation}: any) {
  const {id, isUpdate} = route.params ?? {id: '', isUpdate: false};
  const {
    getContactDataDetailService,
    setContactServiceData,
    updateContactServiceData,
  } = useContact({id});

  const {firstName, lastName, age, photo} =
    getContactDataDetailService.data ?? defaultValue;

  const {control, handleSubmit, reset} = useForm({
    defaultValues: {
      firstName,
      lastName,
      age: age?.toString(),
      photo,
    },
  });
  const onSubmit = (data: any) => {
    if (isUpdate) {
      updateContactServiceData.updateContact({id, ...data});
    } else {
      setContactServiceData.addNewContact(data);
    }
  };

  useEffect(() => {
    if (
      (setContactServiceData.response &&
        setContactServiceData.response.status === 'fulfilled') ||
      (updateContactServiceData.response &&
        updateContactServiceData.response.status === 'fulfilled')
    ) {
      navigation.goBack();
    }
  }, [
    id,
    navigation,
    setContactServiceData.response,
    updateContactServiceData.response,
  ]);

  useEffect(() => {
    reset({firstName, lastName, age: age?.toString(), photo});
  }, [age, firstName, lastName, photo, reset]);

  return (
    <View>
      <CustomDialog
        isLoading={getContactDataDetailService.isLoading}
        isVisible={getContactDataDetailService.isLoading}
      />
      <Controller
        control={control}
        rules={{
          required: true,
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <CustomInput
            placeholder={'First Name'}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
        name={'firstName'}
      />
      <Controller
        control={control}
        rules={{
          required: true,
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <CustomInput
            placeholder={'Last Name'}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
        name={'lastName'}
      />
      <Controller
        control={control}
        rules={{
          required: false,
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <CustomInput
            placeholder={'Age'}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
        name={'age'}
      />
      <Controller
        control={control}
        rules={{
          required: false,
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <CustomInput
            placeholder={'Photo'}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
        name={'photo'}
      />
      <View style={styles.buttonContainer}>
        <NormalButton
          title={isUpdate ? 'update' : 'submit'}
          onPress={handleSubmit(onSubmit)}
        />
        <NormalButton
          title={'cancel'}
          type={'outline'}
          onPress={() => navigation.goBack()}
        />
      </View>
    </View>
  );
}
