# Introduction:

- This is react native apps for interview in BTPN.
- This project setup using create react native apps : https://reactnative.dev/docs/0.70/typescript
- And Repository using: Gitlab.

# Technology and Dependencies:

- This apps is made using react-native version 0.72.4.
- For UI dev using react-native-element for creating widget and stylesheet for styling.
- For state management dev using redux toolkit : https://redux-toolkit.js.org/.
- and for middleware developer using redux toolkit query: https://redux-toolkit.js.org/usage/usage-with-typescript.
- For creating form developer using react-hook-form: https://www.react-hook-form.com/.
- and for unit testing developer using jest and react testing library.
- For running in simulator developer using genymotion.

# Getting Started:

- Android:
  - npm run android.
  - if have problem with sdk:
    - you can create "local.properties" in android folder and write "sdk.dir = /Users/[computer_name]/Library/Android/sdk"
- IOS:
  - npm run ios.

# On Going:

- Feature can be increase:

1.  Create storybook for developer documentation.
    2, Create feature offline mode.

- Code can be increase:

1.  Increase coverage unit testing more than 90%.
2.  Running sonar-scanner for check duplicate code, vulnerabilities or issue.
